pub mod client;
pub mod errors;
pub mod game_service_rest_adapter_impl;
pub mod game_service_rest_adapter_trait;
pub mod request;
pub mod response;
