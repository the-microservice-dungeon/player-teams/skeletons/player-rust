pub mod robot_attack_info_dto;
pub mod robot_attributes_dto;
pub mod robot_dto;
pub mod robot_inventory_dto;
pub mod robot_move_planet_info_dto;
pub mod robot_resource_inventory_dto;
pub mod robot_restoration_type;
pub mod robots_revealed_robot_dto;
